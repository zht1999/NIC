'''
    Contributors: Aolin Feng, Jianpin Lin, Dezhao Wang, Ding Ding, Wei Wang, Yueyu Hu, Tong Chen, Chuanmin Jia, Yihang Chen, Ziqing Ge, Haotian Zhang
'''

dict = {
    'GPU' : True,  # use gpu or not
    'SAVE_REC' : True, # save recon image in encoding process or not
    'USE_GEO' : True, # use geometric operation based RDO or not
    'USE_VR_MODEL': True, # use variable-rate model or not; model dir on bitahub: /model/ljp105/NIC_v02_VR_models/
    'USE_PREPROCESSING': True, # use preprocessing or not
    'num_steps': 5, # number of steps for the preprocessing. more number indicates higher compression performance and longer encoding time.
    'CTU_size' : 512, # CTU size
    'USE_MULTI_HYPER': True, # use multi hyper prior model
    'USE_PREDICTOR': True, # use context predictor
    'USE_POSTPROCESSING': True,
    "Post_processing_epochs": 10000,
    'Post_processing_learning_rate':2,
    'Postprocessing_CTU':2048,
    'USE_ACCELERATION': True, # use accelerated inference
}




